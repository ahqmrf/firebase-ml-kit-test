package com.ahqmrf.firebasemlkittest

import com.google.firebase.ml.vision.text.FirebaseVisionText

/**
 * Created by Ariful Hoque Maruf
 * Jr Software Engineer, Brain Station-23 Ltd.
 * https://www.github.com/ahqmrf
 */
class BlockComparator {
    companion object : Comparator<FirebaseVisionText.Block> {
        override fun compare(a: FirebaseVisionText.Block, b: FirebaseVisionText.Block): Int {
            if(a.cornerPoints!![0].y == b.cornerPoints!![0].y) {
                return  a.cornerPoints!![0].x - b.cornerPoints!![0].x
            }
            return  a.cornerPoints!![0].y - b.cornerPoints!![0].y
        }
    }
}